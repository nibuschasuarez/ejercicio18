import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-padre',
  templateUrl: './padre.component.html',
  styleUrls: ['./padre.component.css']
})
export class PadreComponent implements OnInit {

  Objeto1padre = {
    nombre: "La mujer Maravilla",
    aparicion: "02/06/2017",
    casa:"DC"
  };

  Objeto2padre = {
    desayuno: 'Avena',
    almuerzo: 'Majau',
    cena: 'tortitas de carne'
  }
  
  constructor() { }

  ngOnInit(): void {
  }
}
